import accountResolver from "./account_resolver.js";

const resolvers = accountResolver;

export default resolvers;